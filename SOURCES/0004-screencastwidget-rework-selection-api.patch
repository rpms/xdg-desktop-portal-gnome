From 0f6957254e32ab6dc1c132c0f7623bbac31309f6 Mon Sep 17 00:00:00 2001
From: Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
Date: Wed, 10 Nov 2021 19:54:39 -0300
Subject: [PATCH] screencastwidget: Rework selection APIs

Instead of passing data around using GVariants, add
properly types variables and structures all around.
This will vastly simplify future commits introducing
screencast stream restore.
---
 src/gnomescreencast.c     |  51 ++++++-------
 src/gnomescreencast.h     |   8 +-
 src/remotedesktop.c       |  40 +++++-----
 src/remotedesktopdialog.c |  35 ++++-----
 src/screencast.c          | 151 +++++++++++++++++---------------------
 src/screencast.h          |  12 +++
 src/screencastdialog.c    |  15 ++--
 src/screencastwidget.c    | 132 +++++++++++++++------------------
 src/screencastwidget.h    |   3 +-
 9 files changed, 203 insertions(+), 244 deletions(-)

diff --git a/src/gnomescreencast.c b/src/gnomescreencast.c
index abf5b53..3f8539c 100644
--- a/src/gnomescreencast.c
+++ b/src/gnomescreencast.c
@@ -274,10 +274,10 @@ cursor_mode_to_gnome_cursor_mode (ScreenCastCursorMode cursor_mode)
 }
 
 static gboolean
-gnome_screen_cast_session_record_window (GnomeScreenCastSession *gnome_screen_cast_session,
-                                         const guint64 id,
-                                         ScreenCastSelection *select,
-                                         GError **error)
+gnome_screen_cast_session_record_window (GnomeScreenCastSession  *gnome_screen_cast_session,
+                                         Window                  *window,
+                                         ScreenCastSelection     *select,
+                                         GError                 **error)
 {
   OrgGnomeMutterScreenCastSession *session_proxy =
     gnome_screen_cast_session->proxy;
@@ -292,7 +292,7 @@ gnome_screen_cast_session_record_window (GnomeScreenCastSession *gnome_screen_ca
   g_variant_builder_init (&properties_builder, G_VARIANT_TYPE_VARDICT);
   g_variant_builder_add (&properties_builder, "{sv}",
                          "window-id",
-                         g_variant_new_uint64 (id));
+                         g_variant_new_uint64 (window_get_id (window)));
   if (select->cursor_mode)
     {
       uint32_t gnome_cursor_mode;
@@ -356,10 +356,10 @@ gnome_screen_cast_session_record_window (GnomeScreenCastSession *gnome_screen_ca
 }
 
 static gboolean
-gnome_screen_cast_session_record_monitor (GnomeScreenCastSession *gnome_screen_cast_session,
-                                          const char *connector,
-                                          ScreenCastSelection *select,
-                                          GError **error)
+gnome_screen_cast_session_record_monitor (GnomeScreenCastSession  *gnome_screen_cast_session,
+                                          Monitor                 *monitor,
+                                          ScreenCastSelection     *select,
+                                          GError                 **error)
 {
   OrgGnomeMutterScreenCastSession *session_proxy =
     gnome_screen_cast_session->proxy;
@@ -370,6 +370,7 @@ gnome_screen_cast_session_record_monitor (GnomeScreenCastSession *gnome_screen_c
   OrgGnomeMutterScreenCastStream *stream_proxy;
   GnomeScreenCastStream *stream;
   GVariant *parameters;
+  const char *connector;
 
   g_variant_builder_init (&properties_builder, G_VARIANT_TYPE_VARDICT);
   if (select->cursor_mode)
@@ -383,6 +384,7 @@ gnome_screen_cast_session_record_monitor (GnomeScreenCastSession *gnome_screen_c
     }
   properties = g_variant_builder_end (&properties_builder);
 
+  connector = monitor_get_connector (monitor);
   if (!org_gnome_mutter_screen_cast_session_call_record_monitor_sync (session_proxy,
                                                                       connector,
                                                                       properties,
@@ -436,40 +438,29 @@ gnome_screen_cast_session_record_monitor (GnomeScreenCastSession *gnome_screen_c
 }
 
 gboolean
-gnome_screen_cast_session_record_selections (GnomeScreenCastSession *gnome_screen_cast_session,
-                                             GVariant *selections,
-                                             ScreenCastSelection *select,
-                                             GError **error)
+gnome_screen_cast_session_record_selections (GnomeScreenCastSession  *gnome_screen_cast_session,
+                                             GPtrArray               *streams,
+                                             ScreenCastSelection     *select,
+                                             GError                 **error)
 {
-  GVariantIter selections_iter;
-  GVariant *selection;
+  guint i;
 
-  g_variant_iter_init (&selections_iter, selections);
-  while ((selection = g_variant_iter_next_value (&selections_iter)))
+  for (i = 0; i < streams->len; i++)
     {
-      ScreenCastSourceType source_type;
-      g_autofree char *key = NULL;
-      g_autoptr(GVariant) variant = NULL;
-      guint64 id;
+      ScreenCastStreamInfo *info = g_ptr_array_index (streams, i);
 
-      g_variant_get (selection, "(u?)",
-                     &source_type,
-                     &variant);
-
-      switch (source_type)
+      switch (info->type)
         {
         case SCREEN_CAST_SOURCE_TYPE_MONITOR:
-          key = g_variant_dup_string (variant, NULL);
           if (!gnome_screen_cast_session_record_monitor (gnome_screen_cast_session,
-                                                         key,
+                                                         info->data.monitor,
                                                          select,
                                                          error))
             return FALSE;
           break;
         case SCREEN_CAST_SOURCE_TYPE_WINDOW:
-          id = g_variant_get_uint64 (variant);
           if (!gnome_screen_cast_session_record_window (gnome_screen_cast_session,
-                                                        id,
+                                                        info->data.window,
                                                         select,
                                                         error))
             return FALSE;
diff --git a/src/gnomescreencast.h b/src/gnomescreencast.h
index 06e4e1e..748cf7d 100644
--- a/src/gnomescreencast.h
+++ b/src/gnomescreencast.h
@@ -34,10 +34,10 @@ const char * gnome_screen_cast_session_get_stream_path_from_id (GnomeScreenCastS
 void gnome_screen_cast_session_add_stream_properties (GnomeScreenCastSession *gnome_screen_cast_session,
                                                       GVariantBuilder *streams_builder);
 
-gboolean gnome_screen_cast_session_record_selections (GnomeScreenCastSession *gnome_screen_cast_session,
-                                                      GVariant *selections,
-                                                      ScreenCastSelection *select,
-                                                      GError **error);
+gboolean gnome_screen_cast_session_record_selections (GnomeScreenCastSession  *gnome_screen_cast_session,
+                                                      GPtrArray               *streams,
+                                                      ScreenCastSelection     *select,
+                                                      GError                 **error);
 
 gboolean gnome_screen_cast_session_stop (GnomeScreenCastSession *gnome_screen_cast_session,
                                          GError **error);
diff --git a/src/remotedesktop.c b/src/remotedesktop.c
index a1c688a..46d1dcc 100644
--- a/src/remotedesktop.c
+++ b/src/remotedesktop.c
@@ -104,9 +104,10 @@ static void
 start_done (RemoteDesktopSession *session);
 
 static gboolean
-start_session (RemoteDesktopSession *session,
-               GVariant *selections,
-               GError **error);
+start_session (RemoteDesktopSession     *session,
+               RemoteDesktopDeviceType   device_types,
+               GPtrArray                *streams,
+               GError                  **error);
 
 static void
 cancel_start_session (RemoteDesktopSession *session,
@@ -155,9 +156,10 @@ handle_close (XdpImplRequest *object,
 }
 
 static void
-remote_desktop_dialog_done (GtkWidget *widget,
-                            int dialog_response,
-                            GVariant *selections,
+remote_desktop_dialog_done (GtkWidget                 *widget,
+                            int                        dialog_response,
+                            RemoteDesktopDeviceType    device_types,
+                            GPtrArray                 *streams,
                             RemoteDesktopDialogHandle *dialog_handle)
 {
   int response;
@@ -184,7 +186,7 @@ remote_desktop_dialog_done (GtkWidget *widget,
     {
       g_autoptr(GError) error = NULL;
 
-      if (!start_session (dialog_handle->session, selections, &error))
+      if (!start_session (dialog_handle->session, device_types, streams, &error))
         {
           g_warning ("Failed to start session: %s", error->message);
           response = 2;
@@ -453,9 +455,9 @@ on_gnome_screen_cast_session_ready (GnomeScreenCastSession *gnome_screen_cast_se
 }
 
 static gboolean
-open_screen_cast_session (RemoteDesktopSession *remote_desktop_session,
-                          GVariant *source_selections,
-                          GError **error)
+open_screen_cast_session (RemoteDesktopSession  *remote_desktop_session,
+                          GPtrArray             *streams,
+                          GError               **error)
 {
   OrgGnomeMutterRemoteDesktopSession *session_proxy =
     remote_desktop_session->mutter_session_proxy;
@@ -478,7 +480,7 @@ open_screen_cast_session (RemoteDesktopSession *remote_desktop_session,
                       remote_desktop_session);
 
   if (!gnome_screen_cast_session_record_selections (gnome_screen_cast_session,
-                                                    source_selections,
+                                                    streams,
                                                     &remote_desktop_session->select.screen_cast,
                                                     error))
     return FALSE;
@@ -487,23 +489,19 @@ open_screen_cast_session (RemoteDesktopSession *remote_desktop_session,
 }
 
 static gboolean
-start_session (RemoteDesktopSession *remote_desktop_session,
-               GVariant *selections,
-               GError **error)
+start_session (RemoteDesktopSession     *remote_desktop_session,
+               RemoteDesktopDeviceType   device_types,
+               GPtrArray                *streams,
+               GError                  **error)
 {
   OrgGnomeMutterRemoteDesktopSession *session_proxy;
-  RemoteDesktopDeviceType device_types = 0;
-  g_autoptr(GVariant) source_selections = NULL;
   gboolean need_streams;
 
-  g_variant_lookup (selections, "selected_device_types", "u", &device_types);
   remote_desktop_session->shared.device_types = device_types;
 
-  if (g_variant_lookup (selections, "selected_screen_cast_sources", "@a(us)",
-                        &source_selections))
+  if (streams)
     {
-      if (!open_screen_cast_session (remote_desktop_session,
-                                     source_selections, error))
+      if (!open_screen_cast_session (remote_desktop_session, streams, error))
         return FALSE;
 
       need_streams = TRUE;
diff --git a/src/remotedesktopdialog.c b/src/remotedesktopdialog.c
index a21b008..bae7678 100644
--- a/src/remotedesktopdialog.c
+++ b/src/remotedesktopdialog.c
@@ -61,9 +61,8 @@ static GQuark quark_device_widget_data;
 
 G_DEFINE_TYPE (RemoteDesktopDialog, remote_desktop_dialog, GTK_TYPE_WINDOW)
 
-static void
-add_device_type_selections (RemoteDesktopDialog *dialog,
-                            GVariantBuilder *selections_builder)
+static RemoteDesktopDeviceType
+get_selected_device_types (RemoteDesktopDialog *dialog)
 {
   GList *selected_rows;
   GList *l;
@@ -81,43 +80,36 @@ add_device_type_selections (RemoteDesktopDialog *dialog,
     }
   g_list_free (selected_rows);
 
-  g_variant_builder_add (selections_builder, "{sv}",
-                         "selected_device_types",
-                         g_variant_new_uint32 (selected_device_types));
+  return selected_device_types;
 }
 
 static void
 button_clicked (GtkWidget *button,
                 RemoteDesktopDialog *dialog)
 {
+  RemoteDesktopDeviceType device_types = 0;
+  g_autoptr(GPtrArray) streams = NULL;
   int response;
-  GVariant *selections;
 
   gtk_widget_hide (GTK_WIDGET (dialog));
 
   if (button == dialog->accept_button)
     {
-      GVariantBuilder selections_builder;
       ScreenCastWidget *screen_cast_widget =
         SCREEN_CAST_WIDGET (dialog->screen_cast_widget);
 
       response = GTK_RESPONSE_OK;
-
-      g_variant_builder_init (&selections_builder, G_VARIANT_TYPE_VARDICT);
-
-      add_device_type_selections (dialog, &selections_builder);
-      if (dialog->screen_cast_enable)
-        screen_cast_widget_add_selections (screen_cast_widget,
-                                           &selections_builder);
-      selections = g_variant_builder_end (&selections_builder);
+      device_types = get_selected_device_types (dialog);
+      streams = screen_cast_widget_get_selected_streams (screen_cast_widget);
     }
   else
     {
       response = GTK_RESPONSE_CANCEL;
-      selections = NULL;
+      device_types = 0;
+      streams = NULL;
     }
 
-  g_signal_emit (dialog, signals[DONE], 0, response, selections);
+  g_signal_emit (dialog, signals[DONE], 0, response, device_types, streams);
 }
 
 static void
@@ -356,7 +348,7 @@ remote_desktop_dialog_close_request (GtkWindow *dialog)
 {
   gtk_widget_hide (GTK_WIDGET (dialog));
 
-  g_signal_emit (dialog, signals[DONE], 0, GTK_RESPONSE_CANCEL, NULL);
+  g_signal_emit (dialog, signals[DONE], 0, GTK_RESPONSE_CANCEL, 0, NULL);
 
   return TRUE;
 }
@@ -375,9 +367,10 @@ remote_desktop_dialog_class_init (RemoteDesktopDialogClass *klass)
                                 0,
                                 NULL, NULL,
                                 NULL,
-                                G_TYPE_NONE, 2,
+                                G_TYPE_NONE, 3,
+                                G_TYPE_INT,
                                 G_TYPE_INT,
-                                G_TYPE_VARIANT);
+                                G_TYPE_PTR_ARRAY);
 
   init_screen_cast_widget ();
 
diff --git a/src/screencast.c b/src/screencast.c
index 4ba67aa..2713d26 100644
--- a/src/screencast.c
+++ b/src/screencast.c
@@ -77,11 +77,6 @@ static GnomeScreenCast *gnome_screen_cast;
 GType screen_cast_session_get_type (void);
 G_DEFINE_TYPE (ScreenCastSession, screen_cast_session, session_get_type ())
 
-static gboolean
-start_session (ScreenCastSession *session,
-               GVariant *selections,
-               GError **error);
-
 static gboolean
 is_screen_cast_session (Session *session)
 {
@@ -129,10 +124,76 @@ on_request_handle_close_cb (XdpImplRequest         *object,
   return FALSE;
 }
 
+static void
+on_gnome_screen_cast_session_ready (GnomeScreenCastSession *gnome_screen_cast_session,
+                                    ScreenCastSession      *screen_cast_session)
+{
+  GVariantBuilder streams_builder;
+  GVariantBuilder results_builder;
+
+  g_variant_builder_init (&results_builder, G_VARIANT_TYPE_VARDICT);
+  g_variant_builder_init (&streams_builder, G_VARIANT_TYPE ("a(ua{sv})"));
+
+  gnome_screen_cast_session = screen_cast_session->gnome_screen_cast_session;
+  gnome_screen_cast_session_add_stream_properties (gnome_screen_cast_session,
+                                                   &streams_builder);
+
+  g_variant_builder_add (&results_builder, "{sv}",
+                         "streams",
+                         g_variant_builder_end (&streams_builder));
+
+  xdp_impl_screen_cast_complete_start (XDP_IMPL_SCREEN_CAST (impl),
+                                       screen_cast_session->start_invocation, 0,
+                                       g_variant_builder_end (&results_builder));
+  screen_cast_session->start_invocation = NULL;
+}
+
+static void
+on_gnome_screen_cast_session_closed (GnomeScreenCastSession *gnome_screen_cast_session,
+                                     ScreenCastSession      *screen_cast_session)
+{
+  session_close ((Session *)screen_cast_session);
+}
+
+static gboolean
+start_session (ScreenCastSession  *screen_cast_session,
+               GPtrArray          *streams,
+               GError            **error)
+{
+  GnomeScreenCastSession *gnome_screen_cast_session;
+
+  gnome_screen_cast_session =
+    gnome_screen_cast_create_session (gnome_screen_cast, NULL, error);
+  if (!gnome_screen_cast_session)
+    return FALSE;
+
+  screen_cast_session->gnome_screen_cast_session = gnome_screen_cast_session;
+
+  screen_cast_session->session_ready_handler_id =
+    g_signal_connect (gnome_screen_cast_session, "ready",
+                      G_CALLBACK (on_gnome_screen_cast_session_ready),
+                      screen_cast_session);
+  screen_cast_session->session_closed_handler_id =
+    g_signal_connect (gnome_screen_cast_session, "closed",
+                      G_CALLBACK (on_gnome_screen_cast_session_closed),
+                      screen_cast_session);
+
+  if (!gnome_screen_cast_session_record_selections (gnome_screen_cast_session,
+                                                    streams,
+                                                    &screen_cast_session->select,
+                                                    error))
+    return FALSE;
+
+  if (!gnome_screen_cast_session_start (gnome_screen_cast_session, error))
+    return FALSE;
+
+  return TRUE;
+}
+
 static void
 on_screen_cast_dialog_done_cb (GtkWidget              *widget,
                                int                     dialog_response,
-                               GVariant               *selections,
+                               GPtrArray              *streams,
                                ScreenCastDialogHandle *dialog_handle)
 {
   int response;
@@ -159,7 +220,7 @@ on_screen_cast_dialog_done_cb (GtkWidget              *widget,
     {
       g_autoptr(GError) error = NULL;
 
-      if (!start_session (dialog_handle->session, selections, &error))
+      if (!start_session (dialog_handle->session, streams, &error))
         {
           g_warning ("Failed to start session: %s", error->message);
           response = 2;
@@ -234,82 +295,6 @@ create_screen_cast_dialog (ScreenCastSession     *session,
   return dialog_handle;
 }
 
-static void
-start_done (ScreenCastSession *screen_cast_session)
-{
-  GnomeScreenCastSession *gnome_screen_cast_session;
-  GVariantBuilder streams_builder;
-  GVariantBuilder results_builder;
-
-  g_variant_builder_init (&results_builder, G_VARIANT_TYPE_VARDICT);
-  g_variant_builder_init (&streams_builder, G_VARIANT_TYPE ("a(ua{sv})"));
-
-  gnome_screen_cast_session = screen_cast_session->gnome_screen_cast_session;
-  gnome_screen_cast_session_add_stream_properties (gnome_screen_cast_session,
-                                                   &streams_builder);
-
-  g_variant_builder_add (&results_builder, "{sv}",
-                         "streams",
-                         g_variant_builder_end (&streams_builder));
-
-  xdp_impl_screen_cast_complete_start (XDP_IMPL_SCREEN_CAST (impl),
-                                       screen_cast_session->start_invocation, 0,
-                                       g_variant_builder_end (&results_builder));
-  screen_cast_session->start_invocation = NULL;
-}
-
-static void
-on_gnome_screen_cast_session_ready (GnomeScreenCastSession *gnome_screen_cast_session,
-                                    ScreenCastSession      *screen_cast_session)
-{
-  start_done (screen_cast_session);
-}
-
-static void
-on_gnome_screen_cast_session_closed (GnomeScreenCastSession *gnome_screen_cast_session,
-                                     ScreenCastSession      *screen_cast_session)
-{
-  session_close ((Session *)screen_cast_session);
-}
-
-static gboolean
-start_session (ScreenCastSession *screen_cast_session,
-               GVariant *selections,
-               GError **error)
-{
-  GnomeScreenCastSession *gnome_screen_cast_session;
-  g_autoptr(GVariant) source_selections = NULL;
-
-  gnome_screen_cast_session =
-    gnome_screen_cast_create_session (gnome_screen_cast, NULL, error);
-  if (!gnome_screen_cast_session)
-    return FALSE;
-
-  screen_cast_session->gnome_screen_cast_session = gnome_screen_cast_session;
-
-  screen_cast_session->session_ready_handler_id =
-    g_signal_connect (gnome_screen_cast_session, "ready",
-                      G_CALLBACK (on_gnome_screen_cast_session_ready),
-                      screen_cast_session);
-  screen_cast_session->session_closed_handler_id =
-    g_signal_connect (gnome_screen_cast_session, "closed",
-                      G_CALLBACK (on_gnome_screen_cast_session_closed),
-                      screen_cast_session);
-
-  g_variant_lookup (selections, "selected_screen_cast_sources", "@a(u?)",
-                    &source_selections);
-  if (!gnome_screen_cast_session_record_selections (gnome_screen_cast_session,
-                                                    source_selections,
-                                                    &screen_cast_session->select,
-                                                    error))
-    return FALSE;
-
-  if (!gnome_screen_cast_session_start (gnome_screen_cast_session, error))
-    return FALSE;
-
-  return TRUE;
-}
-
 static gboolean
 handle_start (XdpImplScreenCast     *object,
               GDBusMethodInvocation *invocation,
diff --git a/src/screencast.h b/src/screencast.h
index f5033b2..a9be16b 100644
--- a/src/screencast.h
+++ b/src/screencast.h
@@ -21,6 +21,9 @@
 #include <glib.h>
 #include <gio/gio.h>
 
+#include "displaystatetracker.h"
+#include "shellintrospect.h"
+
 typedef enum _ScreenCastSourceType
 {
   SCREEN_CAST_SOURCE_TYPE_MONITOR = 1,
@@ -42,5 +45,14 @@ typedef struct _ScreenCastSelection
   ScreenCastCursorMode cursor_mode;
 } ScreenCastSelection;
 
+typedef struct
+{
+  ScreenCastSourceType type;
+  union {
+    Monitor *monitor;
+    Window *window;
+  } data;
+} ScreenCastStreamInfo;
+
 gboolean screen_cast_init (GDBusConnection *connection,
                            GError **error);
diff --git a/src/screencastdialog.c b/src/screencastdialog.c
index 56d4d49..3e3b064 100644
--- a/src/screencastdialog.c
+++ b/src/screencastdialog.c
@@ -59,8 +59,8 @@ static void
 button_clicked (GtkWidget *button,
                 ScreenCastDialog *dialog)
 {
+  g_autoptr(GPtrArray) streams = NULL;
   int response;
-  GVariant *selections;
 
   gtk_widget_hide (GTK_WIDGET (dialog));
 
@@ -68,22 +68,17 @@ button_clicked (GtkWidget *button,
     {
       ScreenCastWidget *screen_cast_widget =
         SCREEN_CAST_WIDGET (dialog->screen_cast_widget);
-      GVariantBuilder selections_builder;
 
       response = GTK_RESPONSE_OK;
-
-      g_variant_builder_init (&selections_builder, G_VARIANT_TYPE ("a{sv}"));
-      screen_cast_widget_add_selections (screen_cast_widget,
-                                         &selections_builder);
-      selections = g_variant_builder_end (&selections_builder);
+      streams = screen_cast_widget_get_selected_streams (screen_cast_widget);
     }
   else
     {
       response = GTK_RESPONSE_CANCEL;
-      selections = NULL;
+      streams = NULL;
     }
 
-  g_signal_emit (dialog, signals[DONE], 0, response, selections);
+  g_signal_emit (dialog, signals[DONE], 0, response, streams);
 }
 
 static void
@@ -127,7 +122,7 @@ screen_cast_dialog_class_init (ScreenCastDialogClass *klass)
                                 NULL,
                                 G_TYPE_NONE, 2,
                                 G_TYPE_INT,
-                                G_TYPE_VARIANT);
+                                G_TYPE_PTR_ARRAY);
 
   init_screen_cast_widget ();
 
diff --git a/src/screencastwidget.c b/src/screencastwidget.c
index 454c93e..3119245 100644
--- a/src/screencastwidget.c
+++ b/src/screencastwidget.c
@@ -69,51 +69,6 @@ G_DEFINE_TYPE (ScreenCastWidget, screen_cast_widget, GTK_TYPE_BOX)
  * Auxiliary methods
  */
 
-static gboolean
-add_selections (ScreenCastWidget *widget,
-                GVariantBuilder  *source_selections_builder)
-{
-  GList *selected_monitor_rows;
-  GList *selected_window_rows;
-  GList *l;
-
-  selected_monitor_rows =
-    gtk_list_box_get_selected_rows (GTK_LIST_BOX (widget->monitor_list));
-  selected_window_rows =
-    gtk_list_box_get_selected_rows (GTK_LIST_BOX (widget->window_list));
-  if (!selected_monitor_rows && !selected_window_rows)
-    return FALSE;
-
-  for (l = selected_monitor_rows; l; l = l->next)
-    {
-      GtkWidget *monitor_widget = gtk_list_box_row_get_child (l->data);
-      Monitor *monitor;
-
-      monitor = g_object_get_qdata (G_OBJECT (monitor_widget),
-                                    quark_monitor_widget_data);
-
-      g_variant_builder_add (source_selections_builder, "(us)",
-                             SCREEN_CAST_SOURCE_TYPE_MONITOR,
-                             monitor_get_connector (monitor));
-    }
-  g_list_free (selected_monitor_rows);
-  for (l = selected_window_rows; l; l = l->next)
-    {
-      GtkWidget *window_widget = gtk_list_box_row_get_child (l->data);
-      Window *window;
-
-      window = g_object_get_qdata (G_OBJECT (window_widget),
-                                    quark_window_widget_data);
-
-      g_variant_builder_add (source_selections_builder, "(ut)",
-                             SCREEN_CAST_SOURCE_TYPE_WINDOW,
-                             window_get_id (window));
-    }
-  g_list_free (selected_window_rows);
-
-  return TRUE;
-}
-
 static GtkWidget *
 create_window_widget (Window *window)
 {
@@ -571,25 +526,6 @@ init_screen_cast_widget (void)
   g_type_ensure (screen_cast_widget_get_type ());
 }
 
-void
-screen_cast_widget_add_selections (ScreenCastWidget *widget,
-                                   GVariantBuilder  *selections_builder)
-{
-  GVariantBuilder source_selections_builder;
-
-  g_variant_builder_init (&source_selections_builder, G_VARIANT_TYPE ("a(u?)"));
-  if (!add_selections (widget, &source_selections_builder))
-    {
-      g_variant_builder_clear (&source_selections_builder);
-    }
-  else
-    {
-      g_variant_builder_add (selections_builder, "{sv}",
-                             "selected_screen_cast_sources",
-                             g_variant_builder_end (&source_selections_builder));
-    }
-}
-
 void
 screen_cast_widget_set_app_id (ScreenCastWidget *widget,
                                const char       *app_id)
@@ -649,3 +585,53 @@ screen_cast_widget_set_source_types (ScreenCastWidget     *screen_cast_widget,
   if (__builtin_popcount (source_types) > 1)
     gtk_widget_show (screen_cast_widget->source_type_switcher);
 }
+
+GPtrArray *
+screen_cast_widget_get_selected_streams (ScreenCastWidget *self)
+{
+  ScreenCastStreamInfo *info;
+  g_autoptr(GPtrArray) streams = NULL;
+  g_autoptr(GList) selected_monitor_rows = NULL;
+  g_autoptr(GList) selected_window_rows = NULL;
+  GList *l;
+
+  streams = g_ptr_array_new_with_free_func (g_free);
+
+  selected_monitor_rows =
+    gtk_list_box_get_selected_rows (GTK_LIST_BOX (self->monitor_list));
+  selected_window_rows =
+    gtk_list_box_get_selected_rows (GTK_LIST_BOX (self->window_list));
+
+  if (!selected_monitor_rows && !selected_window_rows)
+    return g_steal_pointer (&streams);
+
+  for (l = selected_monitor_rows; l; l = l->next)
+    {
+      GtkWidget *monitor_widget = gtk_list_box_row_get_child (l->data);
+      Monitor *monitor;
+
+      monitor = g_object_get_qdata (G_OBJECT (monitor_widget),
+                                    quark_monitor_widget_data);
+
+      info = g_new0 (ScreenCastStreamInfo, 1);
+      info->type = SCREEN_CAST_SOURCE_TYPE_MONITOR;
+      info->data.monitor = monitor;
+      g_ptr_array_add (streams, info);
+    }
+
+  for (l = selected_window_rows; l; l = l->next)
+    {
+      GtkWidget *window_widget = gtk_list_box_row_get_child (l->data);
+      Window *window;
+
+      window = g_object_get_qdata (G_OBJECT (window_widget),
+                                    quark_window_widget_data);
+
+      info = g_new0 (ScreenCastStreamInfo, 1);
+      info->type = SCREEN_CAST_SOURCE_TYPE_WINDOW;
+      info->data.window = window;
+      g_ptr_array_add (streams, info);
+    }
+
+  return g_steal_pointer (&streams);
+}
diff --git a/src/screencastwidget.h b/src/screencastwidget.h
index 34360a3..ad95903 100644
--- a/src/screencastwidget.h
+++ b/src/screencastwidget.h
@@ -39,5 +39,4 @@ void screen_cast_widget_set_allow_multiple (ScreenCastWidget *widget,
 void screen_cast_widget_set_source_types (ScreenCastWidget     *screen_cast_widget,
                                           ScreenCastSourceType  source_types);
 
-void screen_cast_widget_add_selections (ScreenCastWidget *widget,
-                                        GVariantBuilder  *selections_builder);
+GPtrArray *screen_cast_widget_get_selected_streams (ScreenCastWidget *self);
